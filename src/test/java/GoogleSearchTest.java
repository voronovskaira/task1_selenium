import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import po.GoogleSearchPO;
import po.SearchResultPO;

import java.util.concurrent.TimeUnit;

public class GoogleSearchTest {
    private static Logger LOG = LogManager.getLogger(GoogleSearchTest.class);
    private String searchKey = "Apple";

    @Test
    public void testGoogleSearch() {
        WebDriver driver = new DriverManager().getDriver();
        GoogleSearchPO googleSearch = new GoogleSearchPO(driver);
        SearchResultPO resultPO = new SearchResultPO(driver);
        try {
            driver.get("https://www.google.com");
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            googleSearch.findApple(searchKey);
            LOG.info("Title is : " + driver.getTitle());
            Assert.assertEquals(driver.getTitle(), searchKey + " - Пошук Google");
            resultPO.clickImagesTab();
            Assert.assertTrue(resultPO.isImagesTabOpen());
        } finally {
            driver.quit();
        }
    }
}


