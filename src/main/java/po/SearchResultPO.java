package po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPO extends BasePage {
    public SearchResultPO(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"hdtb-msb-vis\"]/div[2]")
    WebElement imagesTab;


    public void clickImagesTab() {
        imagesTab.click();
    }

    public boolean isImagesTabOpen() {
        return Boolean.valueOf(imagesTab.getAttribute("aria-selected"));
    }


}
