package po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleSearchPO extends BasePage {
    public GoogleSearchPO(WebDriver driver) {
        super(driver);
    }

    @FindBy(name = "q")
    WebElement searchField;

    public void findApple(String string) {
        this.searchField.sendKeys(string);
        this.searchField.submit();
    }
}
